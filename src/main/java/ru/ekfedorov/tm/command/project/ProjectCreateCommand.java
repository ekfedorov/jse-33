package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Create new project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-create";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "project";
        System.out.println("ENTER DESCRIPTION:");
        @Nullable String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        serviceLocator.getProjectService().add(userId, name, description);
        System.out.println();
    }

}
