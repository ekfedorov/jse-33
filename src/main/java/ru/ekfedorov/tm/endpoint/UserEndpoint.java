package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IUserEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserOneById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findOneById(id).orElse(null);
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) {
        serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login).orElse(null);
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void userUpdate(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().userUpdate(session.getUserId(), firstName, lastName, middleName);
    }

}
