package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.endpoint.IAdminEndpoint;

import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public void projectClear(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear();
    }

}
