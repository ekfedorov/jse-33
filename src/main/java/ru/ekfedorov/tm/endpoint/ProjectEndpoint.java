package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId(), sort);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name).orElse(null);
    }

    @Override
    @WebMethod
    public boolean removeProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public boolean removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
    }

}
