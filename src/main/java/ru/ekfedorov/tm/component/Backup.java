package ru.ekfedorov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.command.data.BackupLoadCommand;
import ru.ekfedorov.tm.command.data.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    private static final int INTERVAL = 15;

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.BACKUP_LOAD);
    }

    @Override
    public void run() {
        bootstrap.parseCommand(BackupSaveCommand.BACKUP_SAVE);
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}
