package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void projectClear(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

}
