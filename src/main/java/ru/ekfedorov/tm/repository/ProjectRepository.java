package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
