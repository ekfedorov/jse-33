package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(@NotNull final String userId) {
        list.stream().filter(predicateByUserId(userId))
                .collect(Collectors.toList())
                .forEach(list::remove);

    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return new ArrayList<>();
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(
            @Nullable final String userId, @NotNull final Comparator<E> comparator
    ) {
        if (isEmpty(userId)) return new ArrayList<>();
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Optional<E> findOneById(
            @Nullable final String userId, @NotNull final String id
    ) {
        if (isEmpty(userId)) return Optional.empty();
        return list.stream()
                .filter(predicateByUserIdAndEntityId(userId, id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findOneByIndex(
            @Nullable final String userId, @NotNull final Integer index
    ) {
        if (isEmpty(userId)) return Optional.empty();
        return Optional.ofNullable(
                list.stream()
                        .filter(predicateByUserId(userId))
                        .collect(Collectors.toList())
                        .get(index)
        );
    }

    @NotNull
    @Override
    public Optional<E> findOneByName(
            @Nullable final String userId, @NotNull final String name
    ) {
        if (isEmpty(userId)) return Optional.empty();
        return list.stream()
                .filter(predicateByNameAndUserId(userId, name))
                .findFirst();
    }

    @NotNull
    public Predicate<E> predicateByNameAndUserId(
            @NotNull final String userId, @NotNull final String name
    ) {
        return e -> userId.equals(e.getUserId()) && name.equals(e.getName());
    }

    @NotNull
    public Predicate<E> predicateByUserId(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    public Predicate<E> predicateByUserIdAndEntityId(
            @NotNull final String userId, @NotNull final String entityId
    ) {
        return e -> userId.equals(e.getUserId()) && entityId.equals(e.getId());
    }

    @Override
    public boolean remove(
            @Nullable final String userId, @NotNull final E entity
    ) {
        if (isEmpty(userId)) return false;
        return findOneById(userId, entity.getId())
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneById(
            @Nullable final String userId, @NotNull final String id
    ) {
        if (isEmpty(userId)) return false;
        return findOneById(userId, id)
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneByIndex(
            @Nullable final String userId, @NotNull final Integer index
    ) {
        if (isEmpty(userId)) return false;
        return findOneByIndex(userId, index)
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneByName(
            @Nullable final String userId, @NotNull final String name
    ) {
        if (isEmpty(userId)) return false;
        return findOneByName(userId, name)
                .map(list::remove)
                .orElse(false);
    }

}
