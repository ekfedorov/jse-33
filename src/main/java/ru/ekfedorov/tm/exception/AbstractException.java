package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.Nullable;

public class AbstractException extends Exception {

    public AbstractException(@Nullable final String message) throws Exception {
        throw new Exception(message);
    }

}
