package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class UserIdIsEmptyException extends AbstractException {

    public UserIdIsEmptyException() throws Exception {
        super("Error! userId is empty...");
    }
}
