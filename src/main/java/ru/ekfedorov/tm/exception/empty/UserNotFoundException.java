package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() throws Exception {
        super("Error! User not found...");
    }
}
