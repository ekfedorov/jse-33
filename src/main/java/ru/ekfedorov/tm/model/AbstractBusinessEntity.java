package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @NotNull
    protected Date created = new Date();

    @Nullable
    protected Date dateFinish;

    @Nullable
    protected Date dateStart;

    @Nullable
    protected String description = "";

    @Nullable
    protected String name = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    private String userId;

    @NotNull
    @Override
    public String toString() {
        return id + ": " + name + " | " + status.getDisplayName();
    }

}

